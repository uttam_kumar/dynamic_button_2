package com.example.uttam.dynamicbutton2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn1,btn2,btn3,btn4,btn5;
    private int[] n = new int[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1=findViewById(R.id.buttonId_1);
        btn2=findViewById(R.id.buttonId_2);
        btn3=findViewById(R.id.buttonId_3);
        btn4=findViewById(R.id.buttonId_4);
        btn5=findViewById(R.id.buttonId_5);


        Random rand = new Random();

        for(int i=0;i<5;i++) {
            n[i] = rand.nextInt((50 - 10) + 1) + 10;
            Log.d("btn", "Clicked Button Number: "+n[i]);
        }

        btn1.setText(String.valueOf(n[0]));
        btn2.setText(String.valueOf(n[1]));
        btn3.setText(String.valueOf(n[2]));
        btn4.setText(String.valueOf(n[3]));
        btn5.setText(String.valueOf(n[4]));



        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);

        btn3.setOnClickListener(this);

        btn4.setOnClickListener(this);

        btn5.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if(v==btn1){
            String s=btn1.getText().toString();
            Log.d("btn1", "Clicked Button Number: "+s);
            Toast.makeText(getApplicationContext(), "Clicked Button Number: "+s, Toast.LENGTH_LONG).show();
        }else if(v==btn2){
            String s=btn2.getText().toString();
            Log.d("btn2", "Clicked Button Number: "+s);
            Toast.makeText(getApplicationContext(), "Clicked Button Number: "+s, Toast.LENGTH_LONG).show();
        }else if(v==btn3){
            String s=btn3.getText().toString();
            Log.d("btn3", "Clicked Button Number: "+s);
            Toast.makeText(getApplicationContext(), "Clicked Button Number: "+s, Toast.LENGTH_LONG).show();
        }else if(v==btn4){
            String s=btn4.getText().toString();
            Log.d("btn4", "Clicked Button Number: "+s);
            Toast.makeText(getApplicationContext(), "Clicked Button Number: "+s, Toast.LENGTH_LONG).show();
        }else if(v==btn5){
            String s=btn5.getText().toString();
            Log.d("btn5", "Clicked Button Number: "+s);
            Toast.makeText(getApplicationContext(), "Clicked Button Number: "+s, Toast.LENGTH_LONG).show();
        }
    }

}
